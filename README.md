# repohomereactnative

this is a linking document for repositories that are stored in my 
bitbucket account. 

This document links to react native.

**here are the repos**

I have written some code related to react native. I mostly use it for training. 

---

1. [https://bitbucket.org/thechalakas/helloworld1n]

2. [https://bitbucket.org/thechalakas/helloworld2]

3. [https://bitbucket.org/thechalakas/helloworld3]

4. [https://bitbucket.org/thechalakas/helloworld3a]

5. [https://bitbucket.org/thechalakas/listdemo]

---

## external links

visit my website here - [the chalakas](http://thechalakas.com)

visit my blog here - [the sanguine tech trainer](https://thesanguinetechtrainer.com)

find me on twiter here - [Jay (twitter)](https://twitter.com)

find me on instagram here - [Jay (instagram)](https://www.instagram.com/jay_instogrm/) 